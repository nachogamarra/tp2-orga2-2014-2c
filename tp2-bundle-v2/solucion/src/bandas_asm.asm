
global bandas_asm

section .data
; Constantes
todos64: DD 64, 64, 64, 64
todos96: DD 96, 96, 96, 96
todos128: DD 128, 128, 128, 128
todos192: DD 192, 192, 192, 192
todos255: DD 255, 255, 255, 255
todos288: DD 288, 288, 288, 288
todos480: DD 480, 480, 480, 480
todos672: DD 672, 672, 672, 672

bits1s: DB 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
; Mascaras
blue_to_int_mask:  DB 0x00, 0x80, 0x80, 0x80, 0x04, 0x80, 0x80, 0x80, 0x08, 0x80, 0x80, 0x80, 0x0C, 0x80, 0x80, 0x80
int_to_blue_mask:  DB 0x00, 0x80, 0x80, 0x80, 0x04, 0x80, 0x80, 0x80, 0x08, 0x80, 0x80, 0x80, 0x0C, 0x80, 0x80, 0x80

green_to_int_mask: DB 0x01, 0x80, 0x80, 0x80, 0x05, 0x80, 0x80, 0x80, 0x09, 0x80, 0x80, 0x80, 0x0D, 0x80, 0x80, 0x80
int_to_green_mask:  DB 0x80, 0x00, 0x80, 0x80, 0x80, 0x04, 0x80, 0x80, 0x80, 0x08, 0x80, 0x80, 0x80, 0x0C, 0x80, 0x80

red_to_int_mask:   DB 0x02, 0x80, 0x80, 0x80, 0x06, 0x80, 0x80, 0x80, 0x0A, 0x80, 0x80, 0x80, 0x0E, 0x80, 0x80, 0x80
int_to_red_mask:   DB 0x80, 0x80, 0x00, 0x80, 0x80, 0x80, 0x04, 0x80, 0x80, 0x80, 0x08, 0x80, 0x80, 0x80, 0x0C, 0x80

alpha_to_int_mask: DB 0x03, 0x80, 0x80, 0x80, 0x07, 0x80, 0x80, 0x80, 0x0B, 0x80, 0x80, 0x80, 0x0F, 0x80, 0x80, 0x80
int_to_alpha_mask: DB 0x80, 0x80, 0x80, 0x00, 0x80, 0x80, 0x80, 0x04, 0x80, 0x80, 0x80, 0x08, 0x80, 0x80, 0x80, 0x0C

section .text

;void bandas_asm    (
    ;unsigned char *src,
    ;unsigned char *dst,
    ;int cols,
    ;int filas,
    ;int src_row_size,
    ;int dst_row_size)

bandas_asm:
    ; RDI <- *src
    ; RSI <- *dst
    ; EDX <- cols
    ; ECX <- filas
    ; R8D <- src_row_size
    ; R9D <- dst_row_size
    PUSH RBP                                   ; Alineada
    MOV RBP, RSP
    PUSH R12                                   ; Desalineada
    PUSH R13                                   ; Alineada

    MOV EDX, EDX                               ; Limpio la parte alta del registro RDX
    MOV ECX, ECX                               ; Limpio la parte alta del registro RCX
    MOV R8D, R8D                               ; Limpio la parte alta del registro R8
    MOV R9D, R9D                               ; Limpio la parte alta del registro R9
    XOR R12, R12                               ; R12 <- pos_count

    MOV RAX, RCX                               ; RAX <- filas
    MUL R9                                     ; RAX <- filas * src_row_size

    MOVDQU XMM8,  [blue_to_int_mask]
    MOVDQU XMM9,  [green_to_int_mask]
    MOVDQU XMM10, [red_to_int_mask]
    MOVDQU XMM11, [alpha_to_int_mask]
    MOVDQU XMM12, [int_to_blue_mask]
    MOVDQU XMM13, [int_to_green_mask]
    MOVDQU XMM14, [int_to_red_mask]
    MOVDQU XMM15, [int_to_alpha_mask]

    MOVDQU XMM7, [bits1s]

    .ciclo:
        CMP R12, RAX
        JE .fin_ciclo
        MOVDQU XMM0, [RDI + R12]                ; XMM0 <- a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b0 | a0 | r0 | g0 | b3
        MOVDQU XMM1, XMM0                       ; XMM1 <- a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b0 | a0 | r0 | g0 | b3
        MOVDQU XMM2, XMM1                       ; XMM2 <- a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b0 | a0 | r0 | g0 | b3
        MOVDQU XMM3, XMM1                       ; XMM3 <- a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b0 | a0 | r0 | g0 | b3
        MOVDQU XMM4, XMM1                       ; XMM4 <- a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b0 | a0 | r0 | g0 | b3
        PSHUFB XMM1, XMM8                       ; XMM1 <- 00 | 00 | 00 | b3 | 00 | 00 | 00 | b2 | 00 | 00 | 00 | b1 | 00 | 00 | 00 | b0
        PSHUFB XMM2, XMM9                       ; XMM2 <- 00 | 00 | 00 | g3 | 00 | 00 | 00 | g2 | 00 | 00 | 00 | g1 | 00 | 00 | 00 | g0
        PSHUFB XMM3, XMM10                      ; XMM3 <- 00 | 00 | 00 | r3 | 00 | 00 | 00 | r2 | 00 | 00 | 00 | r1 | 00 | 00 | 00 | r0
        PSHUFB XMM4, XMM11                      ; XMM4 <- 00 | 00 | 00 | a3 | 00 | 00 | 00 | a2 | 00 | 00 | 00 | a1 | 00 | 00 | 00 | a0
        PADDD XMM1, XMM2                        ; XMM1 <- b3 + g3 | b2 + g2 | b1 + g1 | b0 + g0
        PADDD XMM1, XMM3                        ; XMM1 <- b3 + g3 + r3 | b2 + g2 + r2 | b1 + g1 + r1 | b0 + g0 + r0

        ; caso suma < 96
        MOVDQU XMM2, XMM1                       ; XMM2 <- b3 + g3 + r3 | b2 + g2 + r2 | b1 + g1 + r1 | b0 + g0 + r0
        MOVDQU XMM6, [todos96]                  ; XMM6 <- 96 | 96 | 96 | 96
        PCMPGTD XMM6, XMM2                      ; XMM6 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 96 y 0x0 sino
        PXOR XMM6, XMM7                         ; XMM6 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x >= 96 y 0x0 sino
        PAND XMM1, XMM6                         ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es SUMA_x si SUMA_x >= 96 y 0x0 sino

        ; caso 96 <= suma < 288
        MOVDQU XMM3, XMM1                       ; XMM3 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es SUMA_x si SUMA_x >= 96 y 0x0 sino
        MOVDQU XMM5, [todos288]                 ; XMM5 <- 288 | 288 | 288 | 288
        PCMPGTD XMM5, XMM3                      ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 288 y 0x0 sino
        MOVDQU XMM1, XMM5                       ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 288 y 0x0 sino
        PAND XMM5, XMM6                         ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si 96 <= SUMA_x < 288 y 0x0 sino
        PAND XMM5, [todos64]                    ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 64 si 96 <= SUMA_x < 288 y 0x0 sino
        PXOR XMM1, XMM7                         ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x >= 288 y 0x0 sino
        MOVDQU XMM6, XMM1                       ; XMM6 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x >= 288 y 0x0 sino
        PAND XMM1, XMM3                         ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es SUMA_x si SUMA_x >= 288 y 0x0 sino
        MOVDQU XMM2, XMM5                       ; XMM2 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es:
                                                        ;    0 si SUMA_x < 96
                                                        ;    64 si 96 <= SUMA_x < 288
                                                        ;    0 sino

        ; caso 288 <= suma < 480
        MOVDQU XMM3, XMM1                       ; XMM3 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es SUMA_x si SUMA_x >= 288 y 0x0 sino
        MOVDQU XMM5, [todos480]                 ; XMM5 <- 480 | 480 | 480 | 480
        PCMPGTD XMM5, XMM3                       ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 480 y 0x0 sino
        MOVDQU XMM1, XMM5                       ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 480 y 0x0 sino
        PAND XMM5, XMM6                         ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si 288 <= SUMA_x < 288 y 0x0 sino
        PAND XMM5, [todos128]                   ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 128 si 288 <= SUMA_x < 480 y 0x0 sino
        PXOR XMM1, XMM7                         ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x >= 480 y 0x0 sino
        MOVDQU XMM6, XMM1                       ; XMM6 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x >= 480 y 0x0 sino
        PAND XMM1, XMM3                         ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es SUMA_x si SUMA_x >= 480 y 0x0 sino
        POR XMM2, XMM5                          ; XMM2 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es:
                                                        ;    0 si SUMA_x < 96
                                                        ;    64 si 96 <= SUMA_x < 288
                                                        ;    128 si 288 <= SUMA_x < 480
                                                        ;    0 sino

        ; caso 480 <= suma < 672
        MOVDQU XMM3, XMM1                       ; XMM3 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es SUMA_x si SUMA_x >= 480 y 0x0 sino
        MOVDQU XMM5, [todos672]                 ; XMM5 <- 672 | 672 | 672 | 672
        PCMPGTD XMM5, XMM3                      ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 672 y 0x0 sino
        MOVDQU XMM1, XMM5                       ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x < 672 y 0x0 sino
        PAND XMM5, XMM6                         ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si 480 <= SUMA_x < 672 y 0x0 sino
        PAND XMM5, [todos192]                   ; XMM5 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 192 si 480 <= SUMA_x < 672 y 0x0 sino
        PXOR XMM1, XMM7                         ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 0xFFFFFFFF si SUMA_x >= 672 y 0x0 sino
        POR XMM2, XMM5                          ; XMM2 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es:
                                                        ;    0 si SUMA_x < 96
                                                        ;    64 si 96 <= SUMA_x < 288
                                                        ;    128 si 288 <= SUMA_x < 480
                                                        ;    192 si 480 <= SUMA_x < 672
                                                        ;    0 sino

        ; caso 672 <= suma
        PAND XMM1, [todos255]                   ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es 255 si 672 <= SUMA_x y 0x0 sino
        POR XMM1, XMM2                          ; XMM1 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es:
                                                        ;    0 si SUMA_x < 96
                                                        ;    64 si 96 <= SUMA_x < 288
                                                        ;    128 si 288 <= SUMA_x < 480
                                                        ;    192 si 480 <= SUMA_x < 672
                                                        ;    255 sino
        MOVDQU XMM2, XMM1                       ; XMM2 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es:
                                                        ;    0 si SUMA_x < 96
                                                        ;    64 si 96 <= SUMA_x < 288
                                                        ;    128 si 288 <= SUMA_x < 480
                                                        ;    192 si 480 <= SUMA_x < 672
                                                        ;    255 sino
        MOVDQU XMM3, XMM1                       ; XMM3 <- X_4 | X_3 | X_2 | X_1
                                                        ; Donde X_x es:
                                                        ;    0 si SUMA_x < 96
                                                        ;    64 si 96 <= SUMA_x < 288
                                                        ;    128 si 288 <= SUMA_x < 480
                                                        ;    192 si 480 <= SUMA_x < 672
                                                        ;    255 sino
        PSHUFB XMM1, XMM12                      ; XMM1 <- 00 | 00 | 00 | b3 | 00 | 00 | 00 | b2 | 00 | 00 | 00 | b0 | 00 | 00 | 00 | b3
        PSHUFB XMM2, XMM13                      ; XMM2 <- 00 | 00 | g3 | 00 | 00 | 00 | g2 | 00 | 00 | 00 | g1 | 00 | 00 | 00 | g0 | 00
        PSHUFB XMM3, XMM14                      ; XMM3 <- 00 | r3 | 00 | 00 | 00 | r2 | 00 | 00 | 00 | r1 | 00 | 00 | 00 | r0 | 00 | 00
        PSHUFB XMM4, XMM15                      ; XMM4 <- a3 | 00 | 00 | 00 | a2 | 00 | 00 | 00 | a1 | 00 | 00 | 00 | a0 | 00 | 00 | 00
        POR XMM1, XMM2                          ; XMM1 <- 00 | 00 | g3 | b3 | 00 | 00 | g2 | b2 | 00 | 00 | g1 | b0 | 00 | 00 | g0 | b3
        POR XMM3, XMM4                          ; XMM3 <- a3 | r3 | 00 | 00 | a2 | r2 | 00 | 00 | a1 | r1 | 00 | 00 | a0 | r0 | 00 | 00
        POR XMM1, XMM3                          ; XMM1 <- a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b0 | a0 | r0 | g0 | b3

        MOVDQU [RSI + R12], XMM1
        ADD R12, 16
        JMP .ciclo
    .fin_ciclo:

    POP R13                                    ; Desalineada
    POP R12                                    ; Alineada
    POP RBP                                    ; Desalineada
    RET
