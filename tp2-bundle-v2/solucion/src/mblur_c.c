
#include "tp2.h"

#define MIN(x,y) ( x < y ? x : y )
#define MAX(x,y) ( x > y ? x : y )

#define P 2

void mblur_c    (
    unsigned char *src,
    unsigned char *dst,
    int cols,
    int filas,
    int src_row_size,
    int dst_row_size)
{
    unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
    unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;
	unsigned char suma_r = 0, suma_b = 0, suma_g = 0;
	
	for (int i = 0; i < filas; i++){
        	for (int j = 0; j < cols; j++){
	        	bgra_t *p_d = (bgra_t*) &dst_matrix[i][j * 4];
//			bgra_t *p_s = (bgra_t*) &src_matrix[i][j * 4];
			if(i<2 || j<2|| i+2 >= filas || j+2 >= cols) {p_d->b = 0; p_d->g = 0; p_d->r = 0;}
			else{
				for(int i_aux = i - 2, j_aux = j-2; i_aux <= i+2; i_aux++, j_aux++){
					bgra_t* p_aux = (bgra_t*)&src_matrix[i_aux][j_aux*4];
					//float aux[3] = {0.2*p_aux->b, 0.2*p_aux->g, 0.2*p_aux->r}
					/*aux[0] = 0.2*p_aux->b;
					aux[1] = 0.2*p_aux->g;
					aux[2] = 0.2*p_aux->r;*/
					suma_b = suma_b + (unsigned char)(0.2*p_aux->b);
					suma_g = suma_g + (unsigned char)(0.2*p_aux->g);
					suma_r = suma_r + (unsigned char)(0.2*p_aux->r);
				}
            		p_d->b = suma_b;
			p_d->g = suma_g;
			p_d->r = suma_r;
			suma_r = 0;
			suma_b = 0;
			suma_g = 0;
			}
        	}
	}
 
}


