
#include "tp2.h"

void bandas_c (
	unsigned char *src,
	unsigned char *dst,
	int cols,
	int filas,
	int src_row_size,
	int dst_row_size
) {
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;

	int i, j;
	for (i = 0; i < filas; i++) {
		for (j = 0; j < src_row_size; j+=4) {
			unsigned int r = src_matrix[i][j];
			unsigned int g = src_matrix[i][j + 1];
			unsigned int b = src_matrix[i][j + 2];
			unsigned int suma = r + g + b;
			unsigned int banda;
			if (suma < 96) {
				banda = 0;
			} else if (suma < 288) {
				banda = 64;
			} else if (suma < 480) {
				banda = 128;
			} else if (suma < 672) {
				banda = 192;
			} else {
				banda = 255;
			}
			dst_matrix[i][j] = banda;
			dst_matrix[i][j + 1] = banda;
			dst_matrix[i][j + 2] = banda;
			dst_matrix[i][j + 3] = src_matrix[i][j + 3];
		}
	}

}
