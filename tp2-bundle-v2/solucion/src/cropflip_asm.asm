global cropflip_asm

section .text
;void tiles_asm(unsigned char *src,
;              unsigned char *dst,
;              int cols,
;              int filas
;              int src_row_size,
;              int dst_row_size,
;              int tamx,
;              int tamy,
;              int offsetx,
;              int offsety);

cropflip_asm:
;   RDI        <- *src,
;   RSI        <- *dst,
;   EDX        <- cols,
;   ECX        <- filas
;   R8D        <- src_row_size,
;   R9D        <- dst_row_size,
;   [RBP + 16] <- tamx,
;   [RBP + 24] <- tamy,
;   [RBP + 32] <- offsetx,
;   [RBP + 40] <- offsety
    PUSH RBP                                   ; Alineada
    MOV RBP, RSP
    PUSH R12                                   ; Desalineada
    PUSH R13                                   ; Alineada
    PUSH R14                                   ; Desalineada
    PUSH R15                                   ; Alineada
%define tamx [RBP + 16]
%define tamy [RBP + 24]
%define offsetx [RBP + 32]
%define offsety [RBP + 40]

    MOV EDX, EDX                               ; Limpio la parte alta de RDX
    MOV ECX, ECX                               ; Limpio la parte alta de RCX
    MOV R8D, R8D                               ; Limpio la parte alta de R8
    MOV R9D, R9D                               ; Limpio la parte alta de R9
    MOV R12D, R12D                             ; Limpio la parte alta de R12
    MOV R13D, R13D                             ; Limpio la parte alta de R13
    MOV EAX, EAX                               ; Limpio la parte alta de EAX
    MOV EAX, offsety                           ; RAX <- offsety
    ADD EAX, tamy                              ; RAX <- offsety + tamy
    DEC EAX                                    ; RAX <- offsety + tamy - 1
    MUL R8D                                    ; RAX <- (offsety + tamy - 1) * src_row_size
    ADD RDI, RAX                               ; src += (offsety + tamy - 1) * src_row_size
    MOV R12D, offsetx                          ; R12 <- offsetx
    SHL R12, 2                                 ; R12 <- offsetx * 4
    ADD RDI, R12                               ; src + (offsety + tamy - 1) * src_row_size += offsetx * 4

    MOV R13D, tamy                             ; R13 <- tamy
    XOR R14, R14                               ; R14 <- filas_count = 0

	; int i, j;
	; for (i = 0; i < tamx * 4; i++) {
	; 	for (j = 0; j < tamy; j++) {
	; 		dst_matrix[j][i] = src_matrix[offsety + tamy - j][i + offsetx*4];
	; 	}
	; }
    .ciclo_filas:
    ; while filas_count < tamy do
        CMP R14, R13
        JGE .fin_ciclo_filas
        XOR R15, R15                                ; R15 <- cols_count = 0
        .ciclo_columnas:
        ; while cols_count < dst_row_size do
            CMP R15, R9
            JGE .fin_ciclo_columnas
            ; Por fetch:
            MOVDQU XMM0, [RDI + R15]                ; XMM0 <- 4 pixeles a partir de  src_matrix[offsety + tamy - j][(i + offsetx)*4]
            MOVDQU [RSI + R15], XMM0
            ADD R15, 16
            JMP .ciclo_columnas
        ; end while
        .fin_ciclo_columnas:
        INC R14
        SUB RDI, R8
        ADD RSI, R9
        JMP .ciclo_filas
    ; end while
    .fin_ciclo_filas:

    POP R15                                    ; Desalineada
    POP R14                                    ; Alineada
    POP R13                                    ; Desalineada
    POP R12                                    ; Alineada
    POP RBP                                    ; Desalineada
    RET
