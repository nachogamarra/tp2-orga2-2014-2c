
#include "tp2.h"
#include <math.h>

double calcular_coef(int i, int j, int filas, int cols){
	double res = 0.0;
	int aux = floor((i*255)/filas);
	int aux2 = floor((j*255)/cols);
	res = (double)(aux^aux2);
	return (res/255);
}

void sierpinski_c    (
	unsigned char *src,
	unsigned char *dst,
	int cols,
	int filas,
	int src_row_size,
	int dst_row_size)
{
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;

	for (int i_d = 0, i_s = 0; i_d < filas; i_d++, i_s++) {
		for (int j_d = 0, j_s = 0; j_d < cols; j_d++, j_s++) {
			bgra_t *p_s = (bgra_t*)&src_matrix[i_s][j_s*4];
			bgra_t *p_d = (bgra_t*)&dst_matrix[i_d][j_d*4];
			*p_d = *p_s;
			double coef = calcular_coef(i_d,j_d,filas,cols);
			p_d->b = coef*(p_s->b);
			p_d->g = coef*(p_s->g);
			p_d->r = coef*(p_s->r);
		}
	}
}