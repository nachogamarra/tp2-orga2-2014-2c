
global mblur_asm

%define i r10d
%define j r11d
%define src rdi
%define dst rsi
%define rows ecx
%define cols edx
%define src_row_size r8d
%define dst_row_size r9d

section .data
cero_veinte: dd 0.2, 0.2, 0.2, 0.2
pshufbsegundafila: db 4, 5, 6, 7, 8,9,10,11,12,13,14,15,255,255,255,255
pshufbsegundafila2: db 255,255,255,255,255,255,255,255,255,255,255,255,0,1,2,3
pshufbtercerafila: db 8,9,10,11,12,13,14,15,255,255,255,255,255,255,255,255
pshufbtercerafila2: db 255,255,255,255,255,255,255,255,0,1,2,3,4,5,6,7
pshufbcuartafila: db 12,13,14,15,255,255,255,255,255,255,255,255,255,255,255,255
pshufbcuartafila2: db 255,255,255,255,0,1,2,3,4,5,6,7,8,9,10,11


section .text
;void mblur_asm    (
	;unsigned char *src,
	;unsigned char *dst,
	;int filas,
	;int cols,
	;int src_row_size,
	;int dst_row_size)

;inputs
;rdi = src
;rsi = dst
;edx = filas
;ecx = cols
;r8d = src_row_size
;r9d = dst_row_size

mblur_asm:
	push rbp
	mov rbp, rsp
	push rbx
	push r12
	push r13
	pxor xmm0, xmm0
	pxor xmm11, xmm11
	pxor xmm12, xmm12
	pxor xmm13, xmm13
	pxor xmm14, xmm14
	pxor xmm15, xmm15
	pxor xmm1, xmm1
	pxor xmm0, xmm0
	pxor xmm0, xmm0
	pxor xmm0, xmm0
	pxor xmm0, xmm0
	pxor xmm0, xmm0
	pxor xmm0, xmm0
	mov i, 0
	mov j, 0
	mov r12, 0
	mov rbx, 0
	mov ebx, dst_row_size
	sub ebx, 8
	mov eax, rows
	sub eax, 2
filas:
	cmp i, rows
	je fin_mblur_asm
	cmp i, eax
	je primeras_y_ultimas_filas
	jg primeras_y_ultimas_filas
	cmp i, 2
	jl primeras_y_ultimas_filas
	pxor xmm15, xmm15
	movq [rsi], xmm15
	movq [rsi + rbx], xmm15
	add rsi, 8
	add rdi, 8
	add j, 8
columnas:
	mov r12d, dst_row_size
	;neg r12d
	shl r12d, 1
	add r12d, 8
	mov r13, rdi
	sub r13, r12
	;add r12, rdi
	;mov r13, 2
	;neg r13
	movdqu xmm0, [r13]
	movdqu xmm11, xmm0
	movdqu xmm13, xmm11
	punpckhbw xmm13, xmm15
	punpcklbw xmm11, xmm15
	movdqu xmm14, xmm13
	punpckhwd xmm14, xmm15
	punpcklwd xmm13, xmm15
	movdqu xmm12, xmm11
	punpckhwd xmm12, xmm15
	punpcklwd xmm11, xmm15
	cvtdq2ps xmm11, xmm11
	cvtdq2ps xmm12, xmm12
	cvtdq2ps xmm13, xmm13
	cvtdq2ps xmm14, xmm14
	movdqu xmm10, [cero_veinte]
	mulps xmm11, xmm10
	mulps xmm12, xmm10
	mulps xmm13, xmm10
	mulps xmm14, xmm10
	cvttps2dq xmm11, xmm11
	cvttps2dq xmm12, xmm12
	cvttps2dq xmm13, xmm13
	cvttps2dq xmm14, xmm14
	packusdw xmm13, xmm14
	packusdw xmm11, xmm12
	packuswb xmm11, xmm13
	movdqu xmm0, xmm11

	mov r12d, dst_row_size
	;shl r12d, 1
	add r12d, 8
	mov r13, rdi
	sub r13, r12
	;add r12, rdi
	;mov r13, 2
	;neg r13
	movdqu xmm11, [r13]
	movdqu xmm10, [r13 + 16]
	movdqu xmm9, [pshufbsegundafila]
	;movdqu xmm11, xmm0
	pshufb xmm11, xmm9
	movdqu xmm9, [pshufbsegundafila2]
	pshufb xmm10, xmm9
	por xmm11, xmm10
	movdqu xmm13, xmm11
	punpckhbw xmm13, xmm15
	punpcklbw xmm11, xmm15
	movdqu xmm14, xmm13
	punpckhwd xmm14, xmm15
	punpcklwd xmm13, xmm15
	movdqu xmm12, xmm11
	punpckhwd xmm12, xmm15
	punpcklwd xmm11, xmm15
	cvtdq2ps xmm11, xmm11
	cvtdq2ps xmm12, xmm12
	cvtdq2ps xmm13, xmm13
	cvtdq2ps xmm14, xmm14
	movdqu xmm10, [cero_veinte]
	mulps xmm11, xmm10
	mulps xmm12, xmm10
	mulps xmm13, xmm10
	mulps xmm14, xmm10
	cvttps2dq xmm11, xmm11
	cvttps2dq xmm12, xmm12
	cvttps2dq xmm13, xmm13
	cvttps2dq xmm14, xmm14
	packusdw xmm13, xmm14
	packusdw xmm11, xmm12
	packuswb xmm11, xmm13
	;movdqu xmm10, xmm11
	;pslldq xmm11, 4
	paddb xmm0, xmm11
	;psrldq xmm10, 12
	;paddb xmm1, xmm10

	;mov r12d, dst_row_size
	;shl r12d, 1
	mov r12d, 8
	mov r13, rdi
	sub r13, r12
	;add r12, rdi
	;mov r13, 2
	;neg r13
	movdqu xmm11, [r13]
	movdqu xmm10, [r13 + 16]
	movdqu xmm9, [pshufbtercerafila]
	;movdqu xmm11, xmm0
	pshufb xmm11, xmm9
	movdqu xmm9, [pshufbtercerafila2]
	pshufb xmm10, xmm9
	paddb xmm11, xmm10
	movdqu xmm13, xmm11
	punpckhbw xmm13, xmm15
	punpcklbw xmm11, xmm15
	movdqu xmm14, xmm13
	punpckhwd xmm14, xmm15
	punpcklwd xmm13, xmm15
	movdqu xmm12, xmm11
	punpckhwd xmm12, xmm15
	punpcklwd xmm11, xmm15
	cvtdq2ps xmm11, xmm11
	cvtdq2ps xmm12, xmm12
	cvtdq2ps xmm13, xmm13
	cvtdq2ps xmm14, xmm14
	movdqu xmm10, [cero_veinte]
	mulps xmm11, xmm10
	mulps xmm12, xmm10
	mulps xmm13, xmm10
	mulps xmm14, xmm10
	cvttps2dq xmm11, xmm11
	cvttps2dq xmm12, xmm12
	cvttps2dq xmm13, xmm13
	cvttps2dq xmm14, xmm14
	packusdw xmm13, xmm14
	packusdw xmm11, xmm12
	packuswb xmm11, xmm13
	;movdqu xmm10, xmm11
	;pslldq xmm11, 8
	paddb xmm0, xmm11
	;psrldq xmm10, 8
	;paddb xmm1, xmm10

	mov r12d, dst_row_size
	;shr r12d, 1
	sub r12d, 8
	mov r13, rdi
	add r13, r12
	;add r12, rdi
	;mov r13, 2
	;neg r13
	movdqu xmm11, [r13]
	movdqu xmm10, [r13 + 16]
	movdqu xmm9, [pshufbcuartafila]
	;movdqu xmm11, xmm0
	pshufb xmm11, xmm9
	movdqu xmm9, [pshufbcuartafila2]
	pshufb xmm10, xmm9
	paddb xmm11, xmm10
	movdqu xmm13, xmm11
	punpckhbw xmm13, xmm15
	punpcklbw xmm11, xmm15
	movdqu xmm14, xmm13
	punpckhwd xmm14, xmm15
	punpcklwd xmm13, xmm15
	movdqu xmm12, xmm11
	punpckhwd xmm12, xmm15
	punpcklwd xmm11, xmm15
	cvtdq2ps xmm11, xmm11
	cvtdq2ps xmm12, xmm12
	cvtdq2ps xmm13, xmm13
	cvtdq2ps xmm14, xmm14
	movdqu xmm10, [cero_veinte]
	mulps xmm11, xmm10
	mulps xmm12, xmm10
	mulps xmm13, xmm10
	mulps xmm14, xmm10
	cvttps2dq xmm11, xmm11
	cvttps2dq xmm12, xmm12
	cvttps2dq xmm13, xmm13
	cvttps2dq xmm14, xmm14
	packusdw xmm13, xmm14
	packusdw xmm11, xmm12
	packuswb xmm11, xmm13
	;movdqu xmm10, xmm11
	;pslldq xmm11, 12
	paddb xmm0, xmm11
	;psrldq xmm10, 4
	;paddb xmm1, xmm10

	mov r12d, dst_row_size
	shl r12d, 1
	sub r12d, 8
	mov r13, rdi
	add r13, r12
	;add r12, rdi
	;mov r13, 2
	;neg r13
	;movdqu xmm11, [r13]
	movdqu xmm11, [r13 + 16]
	;movdqu xmm9, [pshufbcuartafila]
	;movdqu xmm11, xmm0
	;pshufb xmm11, xmm9
	;movdqu xmm9, [pshufbcuartafila2]
	;pshufb xmm10, xmm9
	;padd xmm11, xmm10
	movdqu xmm13, xmm11
	punpckhbw xmm13, xmm15
	punpcklbw xmm11, xmm15
	movdqu xmm14, xmm13
	punpckhwd xmm14, xmm15
	punpcklwd xmm13, xmm15
	movdqu xmm12, xmm11
	punpckhwd xmm12, xmm15
	punpcklwd xmm11, xmm15
	cvtdq2ps xmm11, xmm11
	cvtdq2ps xmm12, xmm12
	cvtdq2ps xmm13, xmm13
	cvtdq2ps xmm14, xmm14
	movdqu xmm10, [cero_veinte]
	mulps xmm11, xmm10
	mulps xmm12, xmm10
	mulps xmm13, xmm10
	mulps xmm14, xmm10
	cvttps2dq xmm11, xmm11
	cvttps2dq xmm12, xmm12
	cvttps2dq xmm13, xmm13
	cvttps2dq xmm14, xmm14
	packusdw xmm13, xmm14
	packusdw xmm11, xmm12
	packuswb xmm11, xmm13
	;movdqu xmm10, xmm11
	;pslldq xmm11, 4
	paddb xmm0, xmm11
	movdqu [rsi], xmm0

	add rdi, 16
	add rsi, 16
	add j, 16
	mov r12d, dst_row_size
	sub r12d, j
	cmp r12d, 8
	jne columnas
	mov j, 0
	add i, 1
	add rdi, 8
	add rsi, 8
	jmp filas
	
fin_mblur_asm:
	pop r13
	pop r12
	pop rbx
	pop rbp

    ret

primeras_y_ultimas_filas:
	pxor xmm0, xmm0
ciclo:
	movdqu [rsi], xmm0
	add rdi, 16
	add rsi, 16
	add j, 16
	cmp j, dst_row_size
	jne ciclo
	mov j, 0
	add i, 1
	jmp filas
 
