
#include "tp2.h"


void cropflip_c    (
	unsigned char *src,
	unsigned char *dst,
	int cols,
	int filas,
	int src_row_size,
	int dst_row_size,
	int tamx,
	int tamy,
	int offsetx,
	int offsety)
{
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;

	int i, j;
	for (i = 0; i < tamx * 4; i++) {
		for (j = 0; j < tamy; j++) {
			dst_matrix[j][i] = src_matrix[offsety + tamy - j - 1][i + offsetx*4];
		}
	}
}
