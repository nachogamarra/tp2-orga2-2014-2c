global sierpinski_asm

section .data
;unir_registros_pbaja: DW 0x44
double_255: DQ 255, 255
;shift_bytesx1: DW 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 15
shift_bytesx1: DB 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0
shift_bytesx2: DB 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1
shift_bytesx4: DB 4, 5, 6, 7, 12, 9, 10, 11, 8, 13, 14, 15, 0, 1, 2, 3
reordenar_bytes_dw: DB 0, 1, 2, 3, 8, 5, 10, 7, 4, 9, 6, 11, 12, 13, 14, 15
reordenar_bytes_wb: DB 0, 1, 2, 3, 8, 9, 10, 11, 4, 5, 6, 7, 12, 13, 14, 15

section .text

;void sierpinski_asm (unsigned char *src,
;                     unsigned char *dst,
;                     int cols, int rows,
;                     int src_row_size,
;                     int dst_row_size)

sierpinski_asm:
;              rdi : unsigned char *src
;              rsi : unsigned char *dst,
;              edx : int cols,
;              ecx : int rows,
;              r8d : int src_row_size,
;              r9d : int dst_row_size
    ;STACKFRAME
                ;DESALINEADA
    push rbp    ;ALINEADA
    mov rbp, rsp
    push r12    ;DESALINEADA
    push r13    ;ALINEADA
    push r14    ;DESALINEADA
    push r15    ;ALINEADA

    ;Acomodo parámetros:
    mov edx,edx
    mov ecx,ecx
    mov r8d,r8d
    mov r9d,r9d

;INICIALIZAR VALORES

    ;multiplico cols x 4
    mov edx, edx
    shl rdx, 2

    mov r12, rdx                            ; salvo cols
    mov rax, rdx                            ; (4xcols) % 16
    xor rdx, rdx
    mov r10, 16
    div r10                                 ; en rdx esta el modulo
    xchg r12, rdx                           ; rdx = 4xcols, r12 = modulo

    xor r10, r10                            ; indice_filas
    .recorrer_filas:
    xor r11, r11                            ; indice_columnas

    .recorrer_columnas:
        movdqu xmm1, [rdi+r11]              ; Fetcheo los primeros 5 pixels, el 5to se descarta

                                            ; xmm1 = a4 | r4 | g4 | b4 | a3 | r3 | g3 | b3 | a2 | r2 | g2 | b2 | a1 | r1 | g1 | b1

        ;ahora hay que desempaquetar los pixeles de la imagen para poder hacer la multiplicacion
        ;de byte a dqword

        ;byte a word
        pxor xmm3, xmm3
        pxor xmm4, xmm4
        punpcklbw xmm3, xmm1                ;xmm3 = 0 | a2 | 0 | r2 | 0 | g2 | 0 |  b2 | 0 |  a1 | 0 |  r1 | 0 |  g1 | 0 |  b1
        punpckhbw xmm4, xmm1                ;xmm4 = 0 | a4 | 0 | r4 | 0 | g4 | 0 |  b4 | 0 |  a3 | 0 |  r3 | 0 |  g3 | 0 |  b3
        
        ;acomodar datos para word para que quede little endian
        pshufb xmm3, [shift_bytesx1]
        pshufb xmm4, [shift_bytesx1]

        ;word a dword
        pxor xmm5, xmm5
        pxor xmm6, xmm6
        punpcklwd xmm5, xmm3                ;xmm5 =  0 |  a1 | 0 |  r1 | 0 |  g1 | 0 |  b1
        punpckhwd xmm6, xmm3                ;xmm6 =  0 |  a2 | 0 |  r2 | 0 |  g2 | 0 |  b2

        ;acomodar datos para doble word para que quede little endian
        pshufb xmm5, [shift_bytesx2]
        pshufb xmm6, [shift_bytesx2]

        pxor xmm7, xmm7
        pxor xmm8, xmm8
        punpcklwd xmm7, xmm4                ;xmm7 =  0 |  a3 | 0 |  r3 | 0 |  g3 | 0 |  b3
        punpckhwd xmm8, xmm4                ;xmm8 =  0 |  a4 | 0 |  r4 | 0 |  g4 | 0 |  b4
        
        ;acomodar datos para doble word para que quede little endian
        pshufb xmm7, [shift_bytesx2]
        pshufb xmm8, [shift_bytesx2]

        ;dword a dqword
        pxor xmm9, xmm9
        pxor xmm10, xmm10
        punpckldq xmm9, xmm5                ;xmm9 =   0 |  g1 | 0 |  b1
        punpckhdq xmm10, xmm5                ;xmm10 =  0 |  a1 | 0 |  r1

        ;acomodar datos para dq word para que quede little endian
        pshufb xmm9, [shift_bytesx4]
        pshufb xmm10, [shift_bytesx4]

        pxor xmm11, xmm11
        pxor xmm12, xmm12
        punpckldq xmm11, xmm6                ;xmm11 =  0 |  g2 | 0 |  b2
        punpckhdq xmm12, xmm6                ;xmm12 =  0 |  a2 | 0 |  r2 

        ;acomodar datos para dq word para que quede little endian
        pshufb xmm11, [shift_bytesx4]
        pshufb xmm12, [shift_bytesx4]


        pxor xmm13, xmm13
        pxor xmm14, xmm14
        punpckldq xmm13, xmm7                ;xmm13 =  0 |  g3 | 0 |  b3
        punpckhdq xmm14, xmm7                
        
        ;acomodar datos para dq word para que quede little endian
        pshufb xmm13, [shift_bytesx4]
        pshufb xmm14, [shift_bytesx4]

        pxor xmm15, xmm15
        pxor xmm0, xmm0
        punpckldq xmm15, xmm8                ;xmm15 =  0 |  g4 | 0 |  b4
        punpckhdq xmm0, xmm8                ;xmm0 =  0 |  a4 | 0 |  r4

        ;acomodar datos para dq word para que quede little endian
        pshufb xmm15, [shift_bytesx4]
        pshufb xmm0, [shift_bytesx4]

        ;convertir los dqwords a double float
        cvtdq2pd xmm9, xmm9                  ;xmm9 =   (double) g1 | (double) b1      
        cvtdq2pd xmm10, xmm10                ;xmm10 = (double) a1 | (double)  r1
        cvtdq2pd xmm11, xmm11                ;xmm11 = (double) g2 | (double)  b2
        cvtdq2pd xmm12, xmm12                ;xmm12 = (double) a2 | (double)  r2 
        cvtdq2pd xmm13, xmm13                ;xmm13 = (double) g3 | (double)  b3
        cvtdq2pd xmm14, xmm14                ;xmm14 = (double) a3 | (double)  r3 
        cvtdq2pd xmm15, xmm15                ;xmm15 = (double) g4 | (double)  b4
        cvtdq2pd xmm0, xmm0                  ;xmm0 =  (double) a4 |  (double) r4

        ; ;calculo del coeficiente    
        xorpd xmm2, xmm2
        xorpd xmm3, xmm3
        xorpd xmm4, xmm4
        xorpd xmm5, xmm5
        xorpd xmm6, xmm6
                            ;hay que tener 4 versiones de coef: 
                            ;porque como estamos procesando varios pixeles a la vez,
                            ;los indices correspondientes a la columna son distintos entre los 4

        ;como es necesario modificar los indices, hay que hacer una copia para no poerder los indices originales

        mov r13, r10                        ;r13 = indice_filas
        mov r14, r11                        ;r14 = indice_columnas

        cvtsi2sd xmm2, r13                  ;xmm2 = indice_filas en la parte baja
        shufpd xmm2, xmm2, 0h               ;xmm2 = indice_filas | indice_filas

        cvtsi2sd xmm3, r14                  ;xmm2 = indice_colmumnas en la parte baja
        shufpd xmm3, xmm3, 0h               ;xmm2 = indice_columnas | indice_columnas

        add r14, 4
        cvtsi2sd xmm4, r14                  ;xmm2 = indice_columnas + 1 en la parte baja
        shufpd xmm4, xmm4, 0h               ;xmm2 = indice_columnas + 1 | indice_columnas + 1

        add r14, 4
        cvtsi2sd xmm5, r14                  ;xmm2 = indice_columnas + 2 en la parte baja
        shufpd xmm5, xmm5, 0h               ;xmm2 = indice_columnas + 2 | indice_columnas + 2

        add r14, 4
        cvtsi2sd xmm6, r14                  ;xmm2 = indice_columnas + 3 en la parte baja
        shufpd xmm6, xmm6, 0h               ;xmm2 = indice_columnas + 3 | indice_columnas + 3

        cvtpi2pd xmm7, [double_255]         ;xmm7 = (double) 255 | (double) 255
        shufpd xmm7, xmm7, 0h

        mulpd xmm2, xmm7                    ;xmm2 *= 255
        mulpd xmm3, xmm7                    ;xmm3 *= 255
        mulpd xmm4, xmm7                    ;xmm4 *= 255
        mulpd xmm5, xmm7                    ;xmm5 *= 255
        mulpd xmm6, xmm7                    ;xmm6 *= 255

        cvtsi2sd xmm8, rcx                  ;xmm8 = filas en la parte baja
        shufpd xmm8, xmm8, 0h               ;xmm8 = filas | filas
        divpd xmm2, xmm8                    ;xmm2 = (indice_filas * 255) / filas | (indice_filas * 255) / filas

        cvtsi2sd xmm8, rdx                  ;xmm8 = cols en la parte baja
        shufpd xmm8, xmm8, 0h               ;xmm8 = cols | cols
        divpd xmm3, xmm8                    ;xmm3 =     (indice_columnas * 255) / cols |     (indice_columnas * 255) / cols
        divpd xmm4, xmm8                    ;xmm4 = (indice_columnas + 1 * 255) / cols | (indice_columnas + 1 * 255) / cols
        divpd xmm5, xmm8                    ;xmm5 = (indice_columnas + 2 * 255) / cols | (indice_columnas + 2 * 255) / cols
        divpd xmm6, xmm8                    ;xmm6 = (indice_columnas + 3 * 255) / cols | (indice_columnas + 3 * 255) / cols

        ; truncar la division
        cvttpd2dq xmm2, xmm2                ;xmm2 =  (basura) | (basura) | (indice_filas * 255) / filas | (indice_filas * 255) / filas
        cvttpd2dq xmm3, xmm3                ;xmm3 =  (basura) | (basura) | (indice_columnas * 255) / cols | (indice_columnas * 255) / cols
        cvttpd2dq xmm4, xmm4                ;xmm4 =  (basura) | (basura) | (indice_columnas + 1 * 255) / cols | (indice_columnas + 1 * 255) / cols
        cvttpd2dq xmm5, xmm5                ;xmm5 =  (basura) | (basura) | (indice_columnas + 2 * 255) / cols | (indice_columnas + 2 * 255) / cols
        cvttpd2dq xmm6, xmm6                ;xmm6 =  (basura) | (basura) | (indice_columnas + 3 * 255) / cols | (indice_columnas + 3 * 255) / cols

        pxor xmm3, xmm2                     ;xmm3 ^= xmm2
        pxor xmm4, xmm2                     ;xmm4 ^= xmm2
        pxor xmm5, xmm2                     ;xmm5 ^= xmm2
        pxor xmm6, xmm2                     ;xmm6 ^= xmm2

        ;convertir a double para dividir por 255
        cvtdq2pd xmm3, xmm3                     ;xmm3 = (double) xmm3
        cvtdq2pd xmm4, xmm4                     ;xmm4 = (double) xmm4
        cvtdq2pd xmm5, xmm5                     ;xmm5 = (double) xmm5
        cvtdq2pd xmm6, xmm6                     ;xmm6 = (double) xmm6

        divpd xmm3, xmm7                        ;xmm3 = xmm3 / 255
        divpd xmm4, xmm7                        ;xmm4 = xmm4 / 255
        divpd xmm5, xmm7                        ;xmm5 = xmm5 / 255
        divpd xmm6, xmm7                        ;xmm6 = xmm6 / 255

        ;luego en los siguientes registros se encuentran las diferentes versiones de coef

        ;xmm3 = coef (v1) para indice_columnas
        ;xmm4 = coef (v2) para indice_columnas + 1
        ;xmm5 = coef (v3) para indice_columnas + 2
        ;xmm6 = coef (v4) para indice_columnas + 3

        ;multiplicar todos los valores por coef
        mulpd xmm9, xmm3                    ;xmm9 *= coef (v1)
        mulpd xmm10, xmm3                   ;xmm10 *= coef (v1)
        mulpd xmm11, xmm4                   ;xmm11 *= coef (v2)
        mulpd xmm12, xmm4                   ;xmm12 *= coef (v2)
        mulpd xmm13, xmm5                   ;xmm13 *= coef (v3)
        mulpd xmm14, xmm5                   ;xmm14 *= coef (v3)
        mulpd xmm15, xmm6                   ;xmm15 *= coef (v4)
        mulpd xmm0, xmm6                    ;xmm0 *= coef (v4)

        ;convertir todos los doubles a enteros con truncamiento
        ;(estos quedan en la parte baja de cada registro)
        cvttpd2dq xmm9, xmm9                ;xmm9 =  (basura) | (basura) | coef * g1 | coef * b1
        cvttpd2dq xmm10, xmm10              ;xmm10 = (basura) | (basura) | coef * a1 | coef * r1
        cvttpd2dq xmm11, xmm11              ;xmm11 = (basura) | (basura) | coef * g2 | coef * b2
        cvttpd2dq xmm12, xmm12              ;xmm12 = (basura) | (basura) | coef * a2 | coef * r2
        cvttpd2dq xmm13, xmm13              ;xmm13 = (basura) | (basura) | coef * g3 | coef * b3
        cvttpd2dq xmm14, xmm14              ;xmm14 = (basura) | (basura) | coef * a3 | coef * r3
        cvttpd2dq xmm15, xmm15              ;xmm15 = (basura) | (basura) | coef * g4 | coef * b4
        cvttpd2dq xmm0, xmm0                ;xmm0 =  (basura) | (basura) | coef * a4 | coef * r4

        ;RECONSTRUIR LOS PIXELES Y ESCRIBIR EN MEMORIA
        packusdw xmm9, xmm10                ; xmm9 = (basura) | (basura) | (basura) | (basura) | coef * a1 | coef * r1 | coef * g1 | coef * b1
        pshufb xmm9, [reordenar_bytes_dw]
        packusdw xmm11, xmm12               ; xmm11 = (basura) | (basura) | (basura) | (basura) | coef * a2 | coef * r2 | coef * g2 | coef * b2
        pshufb xmm11, [reordenar_bytes_dw]
        packusdw xmm13, xmm14               ; xmm13 = (basura) | (basura) | (basura) | (basura) | coef * a3 | coef * r3 | coef * g3 | coef * b3
        pshufb xmm13, [reordenar_bytes_dw]
        packusdw xmm15, xmm0               ; xmm15 = (basura) | (basura) | (basura) | (basura) | coef * a4 | coef * r4 | coef * g4 | coef * b4
        pshufb xmm15, [reordenar_bytes_dw]

        packuswb xmm9, xmm11                ; xmm9 =  (basura) | coef * a2 | coef * r2 | coef * g2 | coef * b2 | coef * a1 | coef * r1 | coef * g1 | coef * b1
        pshufb xmm9, [reordenar_bytes_wb]
        packuswb xmm13, xmm15               ; xmm11 = (basura) | coef * a4 | coef * r4 | coef * g4 | coef * b4 | coef * a3 | coef * r3 | coef * g3 | coef * b3
        pshufb xmm13, [reordenar_bytes_wb]

        shufps xmm9, xmm13, 0x44
        ; xmm9 = | coef * a4 | coef * r4 | coef * g4 | coef * b4 | coef * a3 | coef * r3 | coef * g3 | coef * b3 | coef * a2 | coef * r2 | coef * g2 | coef * b2 | coef * a1 | coef * r1 | coef * g1 | coef * b1

        ;------------------------------------------------------------------------------------;
        ;                                                                                    ;
        ;   PARA EL INFORME: EJERCICIO 2 CPU VS MEMORIA                                      ;
        ;                                                                                    ;
        ;------------------------------------------------------------------------------------;


        ; CPU
        ; ; 4 instrucciones extra
        ; add rax, r15
        ; sub rax, 2
        ; add r15, 8
        ; sub r15, rax
        ; ; 8 instrucciones extra
        ; add rax, r15
        ; sub rax, 2
        ; add r15, 8
        ; sub r15, rax
        ; ; 16 instrucciones extra
        ; add rax, r15
        ; sub rax, 2
        ; add r15, 8
        ; sub r15, rax
        ; add rax, r15
        ; sub rax, 2
        ; add r15, 8
        ; sub r15, rax

        ; MEMORIA
        ; 4 instrucciones extra
        ; mov r15, [rsp]
        ; mov rax, [rsp]
        ; mov [rsp+8],r15
        ; mov [rsp+8],rax
        ; ; ; 8 instrucciones extra
        ; mov r15, [rsp]
        ; mov rax, [rsp]
        ; mov [rsp+8],r15
        ; mov [rsp+8],rax
        ; ; ; 16 instrucciones extra
        ; mov r15, [rsp]
        ; mov rax, [rsp]
        ; mov [rsp+8],r15
        ; mov [rsp+8],rax
        ; mov r15, [rsp]
        ; mov rax, [rsp]
        ; mov [rsp+8],r15
        ; mov [rsp+8],rax

        ;------------------------------------------------------------------------------------;
        ;                                                                                    ;
        ;   PARA EL INFORME: EJERCICIO 2 CPU VS MEMORIA                                      ;
        ;                                                                                    ;
        ;------------------------------------------------------------------------------------;

        movdqu [rsi+r11], xmm9              ; Escribo en destino los 4 pixeles resultado

        ; if (indice_columnas==0) then add indice_columnas, r12
        ;                         else add indice_columnas, 16
        cmp r12, 0
        je .sumar_16
        cmp r11, 0
        je .sumar_modulo
        .sumar_16:
        add r11, 16
        jmp .sigo
        .sumar_modulo:
        add r11, r12

        .sigo:
        cmp rdx, r11            ; if 4xcols > indice_columnas
        jg .recorrer_columnas

        inc r10                 ; indice_filas++
        add rsi, r9             ; dst += dst + dst_row_size
        add rdi, r8             ; src += src + src_row_size

        cmp rcx, r10
        jg .recorrer_filas

    .fin:
    pop r15
    pop r14
    pop r13
    pop r12
    pop rbp
    ret
