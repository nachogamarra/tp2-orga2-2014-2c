
#include <stdio.h>
#include <string.h>

#include "tp2.h"
#include "tiempo.h"		//descomentar para medir tiempos
static const int CANT_EJECUCIONES_MEDICION_TIEMPO=100;

void sierpinski_asm    (unsigned char* src, unsigned char *dst, int cols, int filas,
                      int src_row_size, int dst_row_size);

void sierpinski_c    (unsigned char *src,unsigned char *dst, int cols, int filas,
                      int src_row_size, int dst_row_size);

typedef void (sierpinski_fn_t) ( unsigned char*, unsigned char*, int, int, int, int);
double varianza(unsigned long long int *tiemposRecientes, double float_promedio_sierpinski);
void leer_params_sierpinski(configuracion_t *config, int argc, char *argv[]) {
}

void aplicar_sierpinski(configuracion_t *config)
{
	sierpinski_fn_t *sierpinski = SWITCH_C_ASM ( config, sierpinski_c, sierpinski_asm ) ;
	buffer_info_t info = config->src;

	unsigned long long int tiemposRecientes [CANT_EJECUCIONES_MEDICION_TIEMPO];
	unsigned long long int sumaTiempos = 0;
	for(int i=0;i<CANT_EJECUCIONES_MEDICION_TIEMPO;i++){
		unsigned long long int momentoInicio, momentoFinalizacion;
		MEDIR_TIEMPO_START(momentoInicio);
		sierpinski(info.bytes, config->dst.bytes, info.width, info.height, info.width_with_padding, config->dst.width_with_padding);
		MEDIR_TIEMPO_STOP(momentoFinalizacion);
		unsigned long long int tiempoEjecucionReciente = momentoFinalizacion - momentoInicio;
		tiemposRecientes[i] = tiempoEjecucionReciente;
		sumaTiempos = sumaTiempos + tiempoEjecucionReciente;
	}
	double float_promedio_sierpinski = (double)sumaTiempos/(double)CANT_EJECUCIONES_MEDICION_TIEMPO;
	printf("Medición de tiempos para %d ejecuciones de filtro Sierpinski, en versión %s\n",CANT_EJECUCIONES_MEDICION_TIEMPO,C_ASM(config));
	printf("Suma de tiempos de las %d ejecuciones: %lld\n",CANT_EJECUCIONES_MEDICION_TIEMPO,sumaTiempos);
	printf("Promedio de tiempo de ejecución: %f\n",float_promedio_sierpinski);
	double varianza_sierpinski = varianza(tiemposRecientes,float_promedio_sierpinski);
	printf("Varianza de tiempo de ejecución: %f\n",varianza_sierpinski);
	/*Para archivos de mediciones*/
	const char* promedio_sierpinski_c = "promedio_sierpinski_c.out";
	const char* varianza_sierpinski_c = "varianza_sierpinski_c.out";
	const char* promedio_sierpinski_asm = "promedio_sierpinski_asm.out";
	const char* varianza_sierpinski_asm = "varianza_sierpinski_asm.out";
	const char* tamanio_imagenes = "tamanio_imagenes.out";

	unsigned int tamanio_imagen = (unsigned int)info.width;

	if (config->tipo_filtro == FILTRO_C)
	{
		FILE * archivo_promedio_sierpinski_c = fopen(promedio_sierpinski_c,"a");
		fprintf (archivo_promedio_sierpinski_c, "%f\n",float_promedio_sierpinski);
		FILE * archivo_varianza_sierpinski_c = fopen(varianza_sierpinski_c,"a");
		fprintf (archivo_varianza_sierpinski_c, "%f\n",varianza_sierpinski);
		FILE * archivo_tamanio_imagenes = fopen(tamanio_imagenes,"a");
		fprintf (archivo_tamanio_imagenes, "%u\n",tamanio_imagen);

	} else {
		FILE * archivo_promedio_sierpinski_asm = fopen(promedio_sierpinski_asm,"a");
		fprintf (archivo_promedio_sierpinski_asm, "%f\n",float_promedio_sierpinski);
		FILE * archivo_varianza_sierpinski_asm = fopen(varianza_sierpinski_asm,"a");
		fprintf (archivo_varianza_sierpinski_asm, "%f\n",varianza_sierpinski);
		FILE * archivo_tamanio_imagenes = fopen(tamanio_imagenes,"a");
		fprintf (archivo_tamanio_imagenes, "%u\n",tamanio_imagen);

	}
		/*Para archivos de mediciones*/
	

}

double varianza(unsigned long long int *tiemposRecientes, double float_promedio_sierpinski)
{
	double res=0;
	double suma=0;
	for (int i=0; i<CANT_EJECUCIONES_MEDICION_TIEMPO; i++)
	{
		suma += (tiemposRecientes[i] - float_promedio_sierpinski)*(tiemposRecientes[i] - float_promedio_sierpinski);
	}
	res = suma/CANT_EJECUCIONES_MEDICION_TIEMPO;
	return res;
}

void ayuda_sierpinski()
{
	printf ( "       * sierpinski\n" );
	printf ( "           Parámetros     : \n"
	         "                         ninguno\n");
	printf ( "           Ejemplo de uso : \n"
	         "                         sierpinski -i c imagen.bmp\n" );
}