\subsection{Filtro \textit{Motion Blur}}
El filtro motion blur toma una imagen fuente y le aplica un efecto de desenfoque de movimiento. En el filtro implementado, dicho desenfoque es de un movimiento de 45 grados, como puede observarse en la figura \ref{mblur_dst}.
\begin{center}
 
\begin{figure}
  \label{}
  \centering
    \subfloat[Imagen fuente]{
 	\label{mblur_source}
 	\includegraphics[scale=0.5]{imagenes/lena.jpg}}
 	%\caption{Imagen fuente: sector a replicar en destino}
   \hspace{0.1em}
     \subfloat[Imagen destino]{
 	\label{mblur_dst}
 	\includegraphics[scale=0.5]{imagenes/sierpinskiLena.jpg}}
 	\caption{Aplicación de Sierpinski}
   
\end{figure}
\end{center}

Se implementaron 2 algoritmos, uno escrito en lenguaje C y otro en Assembler para resolver el problema.\\
Este filtro no recibe parámetros.\\
\subsubsection{Descripción de la implementación en versión \textit{C}}
Para la implementación en C se recurrió al recorrido lineal de la imagen de entrada y de salida, es decir píxel por píxel
de izquierda a derecha de arriba a abajo. 
Para el cálculo de la posición del píxel de origen se recurrió a la fórmula proporcionada por la cátedra en el enunciado:

\begin{center}
 	dst$_{(i,j)}$ = 0.2 * src$_{(i - 2,j - 2)}$ + 0.2 * src$_{(i - 1,j - 1)}$ + 0.2 * src$_{(i,j)}$ + 0.2 * src$_{(i + 1,j + 1)}$ + 0.2 * src$_{(i + 2,j + 2)}$
\end{center}

\paragraph{}
Dado que en los bordes no se puede aplicar el fitlro a los pixeles, debido a la ausencia de vecinos,
entonces
\begin{center}
dst$_{(i,j)} = 0$ si $i < 2 \lor j<2 \lor i>tamy-2 \lor j>tamx(en\ bytes)-2$
\end{center}
\paragraph{}
El pseudocódigo de la implementación es: \\
\vspace{20pt}
\begin{codesnippet}
	\begin{tabbing}
	 \hspace{20pt} \textbf{for} $i = 0$ \textbf{to} $filas$ \\
	\hspace{40pt}	\textbf{for} $j = 0$ \textbf{to} $cols$ \\
	\hspace{50pt}    if\ ($pixelFuente_{ij}$ está en un borde) \\
	\hspace{60pt} $pixelDestino_{ij}$ = (rojo = 0, verde = 0, azul = 0) \\
	\hspace{50pt}    else \\
	\hspace{60pt} $suma_{rgb}$ = (rojo = 0, verde = 0, azul = 0) \\
	\hspace{60pt}	\textbf{for} $k = -2$ \textbf{to} $2$ \\
	\hspace{70pt} $suma_{rgb}$ = $suma_{rgb} + 0.2 * (pixelDestino_{i-k,j-k})$ \\
	\hspace{60pt} \textbf{end for} \\
	\hspace{60pt} $pixelDestino_{ij}$ = $suma_{rgb}$ \\
	\hspace{40pt}	\textbf{end for} \\
	\hspace{20pt} \textbf{end for} \\
	\end{tabbing}
\end{codesnippet}
\subsubsection{Descripción de la implementación en versión \textit{Assembler}}
Para empezar primero fue necesario multiplicar por 4 el parámetro $cols$ ya que está expresado en \textbf{píxeles}
y en los algoritmos es necesario tenerlos expresados en \textbf{bytes} (1 píxel = 4 bytes).

El algoritmo implementado en este caso difiere del implementado en C fundamentalmente en \textbf{la cantidad de datos que se procesan simultáneamente }, ya que aquí se utilizan varios registros XMM, los cuales tienen un tama\~no de 16 bytes, tanto para leer los datos de la imagen de entrada como para procesar y escribir los datos en destino, de esta manera en este filtro se pueden procesar hasta 16 bytes simultaneamente (4 píxeles RGBA). 
Con esto podríamos conjeturar que el funcionamiento de este filtro será 4 veces más veloz que en C. \\
\textbf{Aclaración: } como se mencionó en el enunciado, se asume que la imagen de entrada tiene un ancho múltiplo de 4 y mayor a 32 píxeles. 
Nuestra solución de este filtro en versión Assembler verifica la siguiente secuencia de pasos:
\paragraph{}
\begin{codesnippet}
	\begin{tabbing}
	\hspace{20pt} 2. Por cada fila de la matriz original\{ \\
	\hspace{20pt} 3. \hspace{20pt}	Si es una de las dos primeras o dos últimas filas, escribir 0 en toda la fila.\\
	\hspace{20pt} 4. \hspace{20pt} Caso contrario:\\
	\hspace{20pt} 4. \hspace{40pt} Escribir 0 en los dos primeros y dos últimos píxeles de la fila. \\
	\hspace{20pt} 5. \hspace{40pt} A partir del segundo píxel, mientras haya más píxeles en la fila:\\
	\hspace{20pt} 6. \hspace{60pt} Procesar los siguientes cuatro píxeles de la fila (iteración regular) \\
	\hspace{20pt} 8. \}\\
	\end{tabbing}
\end{codesnippet} 

\paragraph{}
A continuación se detalla el procedimiento seguido por una iteración regular.
	
\textbf{Detalle de una iteración regular:}
\paragraph{}
 Una iteración regular de nuestra implementación procesa simultáneamente cuatro píxeles de una misma fila. 


Consideremos la siguiente sección de la imagen fuente, donde cada $p_ij$ es un píxel con $i$ número de fila y $j$ número de columna:
	    \begin{center}
			\textbf{imagen fuente}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.185em,bitheight=3em,endianness=little]{256}
				\bitboxes{32}{{$p_i-2,j-2$}{$p_i-2,j-1$}{$p_i-2,j$}{$p_i-2,j+1$}{$p_i-2,j+2$}{$p_i-2,j+3$}{$p_i-2,j+4$}{$p_i-2,j+5$}}\\
				\bitboxes{32}{{$p_i-1,j-2$}{$p_i-1,j-1$}{$p_i-1,j$}{$p_i-1,j+1$}{$p_i-1,j+2$}{$p_i-1,j+3$}{$p_i-1,j+4$}{$p_i-1,j+5$}}\\
				\bitboxes{32}{{$p_i,j-2$}{$p_i,j-1$}{$p_i,j$}{$p_i,j+1$}{$p_i,j+1$}{$p_i,j+3$}{$p_i,j+4$}{$p_i,j+5$}}\\
				\bitboxes{32}{{$p_i+1,j-2$}{$p_i+1,j-1$}{$p_i+1,j$}{$p_i+1,j+1$}{$p_i+1,j+2$}{$p_i+1,j+3$}{$p_i+1,j+4$}{$p_i+1,j+5$}}\\
				\bitboxes{32}{{$p_i+2,j-2$}{$p_i+2,j-1$}{$p_i+2,j$}{$p_i+2,j+1$}{$p_i+2,j+2$}{$p_i+2,j+3$}{$p_i+2,j+4$}{$p_i+2,j+5$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
Los píxeles que se utilizan para realizar las operaciones son los que denomino $A_k$, $B_k$, $C_k$ y $D_k$.
	    \begin{center}
			\textbf{imagen fuente}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.185em,bitheight=3em,endianness=little]{256}
				\bitboxes{32}{{$A_1$}{$B_1$}{$C_1$}{$D_1$}{}{}{}{}}\\
				\bitboxes{32}{{}{$A_1$}{$B_1$}{$C_1$}{$D_1$}{}{}{}}\\
				\bitboxes{32}{{}{}{$A_1$}{$B_1$}{$C_1$}{$D_1$}{}{}}\\
				\bitboxes{32}{{}{}{}{$A_1$}{$B_1$}{$C_1$}{$D_1$}{}}\\
				\bitboxes{32}{{}{}{}{}{$A_1$}{$B_1$}{$C_1$}{$D_1$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
Los píxeles de la imagen destino que se obtienen son los que denomino $dst_{i,j}$,$dst_{i,j+1}$,$dst_{i,j+2}$ y $dst_{i,j+3}$.
	    \begin{center}
			\textbf{imagen destino}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.185em,bitheight=3em,endianness=little]{8}
				\bitboxes{32}{{}{}{}{}{}{}{}{}}\\
				\bitboxes{32}{{}{}{}{}{}{}{}{}}\\
				\bitboxes{32}{{}{}{$dst_{i,j}$}{$dst_{i,j+1}$}{$dst_{i,j+2}$}{$dst_{i,j+3}$}{}{}}\\
				\bitboxes{32}{{}{}{}{}{}{}{}{}}\\
				\bitboxes{32}{{}{}{}{}{}{}{}{}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}

Donde:\\
$dst_{i,j} = 0.2 * (A_1 + A_2 + A_3 + A_4 + A_5)$\\
$dst_{i,j+1} = 0.2*(B_1 + B_2 + B_3 + B_4 + B_5)$\\
$dst_{i,j+2} = 0.2*(C_1 + C_2 + C_3 + C_4 + C_5)$\\
$dst_{i,j+3} = 0.2*(D_1 + D_2 + D_3 + D_4 + D_5)$\\
Primero se leen en el registro XMM11
los píxeles de la imagen fuente a partir del píxel$_{i-2,j-2}$. Entonces en XMM11 tengo:
	    \begin{center}
			\textbf{XMM11}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitboxes{32}{{$A_1$}{$B_1$}{$C_1$}{$D_1$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
Luego, es necesario convertir cada uno de los colores del pixel (rojo, verde y azul) a punto flotante de precisión simple para más adelante poder multiplicar estos valores por 0.2.
Entonces realizo los siguientes desempaquetamientos:
\begin{enumerate}
	\item \textbf{De byte a word: } pixeles desde XMM11 a XMM11 y XMM13.

	    \begin{center}
			\textbf{XMM11 (bytes)}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitheader{127,112,96,80,64,48,32,16,0}\\ 
				\bitbox{8}{$A_{1B}$}
				\bitbox{8}{$A_{1G}$}
				\bitbox{8}{$A_{1R}$}
				\bitbox{8}{$A_{1A}$}
				\bitbox{8}{$B_{1B}$}
				\bitbox{8}{$B_{1G}$}
				\bitbox{8}{$B_{1R}$}
				\bitbox{8}{$B_{1A}$}
				\bitbox{8}{$C_{1B}$}
				\bitbox{8}{$C_{1G}$}
				\bitbox{8}{$C_{1R}$}
				\bitbox{8}{$C_{1A}$}
				\bitbox{8}{$D_{1B}$}
				\bitbox{8}{$D_{1G}$}
				\bitbox{8}{$D_{1R}$}
				\bitbox{8}{$D_{1A}$}
			\end{bytefield}\\\vspace{1em}
		\end{center}

		\begin{center}
		\textbf{XMM11 (words)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.30em,bitheight=2em,endianness=little]{128}
			\bitheader{127,112,96,80,64,48,32,16,0}\\ 
				\bitbox{16}{$A_{1B}$}
				\bitbox{16}{$A_{1G}$}
				\bitbox{16}{$A_{1R}$}
				\bitbox{16}{$A_{1A}$}
				\bitbox{16}{$B_{1B}$}
				\bitbox{16}{$B_{1G}$}
				\bitbox{16}{$B_{1R}$}
				\bitbox{16}{$B_{1A}$}
		\end{bytefield}\\\vspace{1em}
		\end{center}

		\begin{center}
		\textbf{XMM13 (words)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.30em,bitheight=2em,endianness=little]{128}
			\bitheader{127,112,96,80,64,48,32,16,0}\\ 
				\bitbox{16}{$C_{1B}$}
				\bitbox{16}{$C_{1G}$}
				\bitbox{16}{$C_{1R}$}
				\bitbox{16}{$C_{1A}$}
				\bitbox{16}{$D_{1B}$}
				\bitbox{16}{$D_{1G}$}
				\bitbox{16}{$D_{1R}$}
				\bitbox{16}{$D_{1A}$}
		\end{bytefield}\\\vspace{1em}
		\end{center}

	\item \textbf{De word a dword: } pixeles desde XMM11 y XMM13 a XMM11, XMM12, XMM13 y XMM14.
    \begin{center}
		\textbf{XMM11 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$A_{1B}$}
				\bitbox{32}{$A_{1G}$}
				\bitbox{32}{$A_{1R}$}
				\bitbox{32}{$A_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}
    \begin{center}
		\textbf{XMM12 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$B_{1B}$}
				\bitbox{32}{$B_{1G}$}
				\bitbox{32}{$B_{1R}$}
				\bitbox{32}{$B_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}
    \begin{center}
		\textbf{XMM13 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$C_{1B}$}
				\bitbox{32}{$C_{1G}$}
				\bitbox{32}{$C_{1R}$}
				\bitbox{32}{$C_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}
    \begin{center}
		\textbf{XMM14 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$D_{1B}$}
				\bitbox{32}{$D_{1G}$}
				\bitbox{32}{$D_{1R}$}
				\bitbox{32}{$D_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}%
Por otra parte, es necesario tener la constante $0.2$ en formato float para poder realizar las cuentas, este numero se almacena en XMM7:
	\begin{center}
		\textbf{XMM10 (float)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
			\bitbox{32}{$0.2$}
			\bitbox{32}{$0.2$}
			\bitbox{32}{$0.2$}
			\bitbox{32}{$0.2$}
		\end{bytefield}\\\vspace{1em}
	\end{center}

	\item \textbf{De dqword a float de doble precisión: } castear pixeles XMM11, XMM12, XMM13 y XMM14 a float usando la instrucción \texttt{CVTDQ2PS}.
\end{enumerate}
Usando la instrucción MULPS se multiplican los registros XMM11, XMM12, XMM13 y XMM14 por 0.2.
    \begin{center}
		\textbf{XMM11 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$0.2 * A_{1B}$}
				\bitbox{32}{$0.2*A_{1G}$}
				\bitbox{32}{$0.2*A_{1R}$}
				\bitbox{32}{$0.2*A_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}
    \begin{center}
		\textbf{XMM12 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$0.2 * B_{1B}$}
				\bitbox{32}{$0.2*B_{1G}$}
				\bitbox{32}{$0.2*B_{1R}$}
				\bitbox{32}{$0.2*B_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}
    \begin{center}
		\textbf{XMM13 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$0.2 * C_{1B}$}
				\bitbox{32}{$0.2*C_{1G}$}
				\bitbox{32}{$0.2*C_{1R}$}
				\bitbox{32}{$0.2*C_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}
    \begin{center}
		\textbf{XMM14 (dwords)}\\\vspace{1em}
		\begin{bytefield}[bitwidth=0.20em,bitheight=2em,endianness=little]{128}
			\bitheader{127,96,64,32,0}\\ 
				\bitbox{32}{$0.2 * D_{1B}$}
				\bitbox{32}{$0.2*D_{1G}$}
				\bitbox{32}{$0.2*D_{1R}$}
				\bitbox{32}{$0.2*D_{1A}$}
		\end{bytefield}\\\vspace{1em}
	\end{center}%
Estos datos se convierten a dword utilizando la instrucción CVTPS2DQ, y se empaquetan dos veces para convertirlos a byte. Luego se guardan en XMM0.
	    \begin{center}
			\textbf{XMM0 (bytes)}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitheader{127,112,96,80,64,48,32,16,0}\\ 
				\bitbox{8}{$0.2*A_{1B}$}
				\bitbox{8}{$0.2*A_{1G}$}
				\bitbox{8}{$0.2*A_{1R}$}
				\bitbox{8}{$0.2*A_{1A}$}
				\bitbox{8}{$0.2*B_{1B}$}
				\bitbox{8}{$0.2*B_{1G}$}
				\bitbox{8}{$0.2*B_{1R}$}
				\bitbox{8}{$0.2*B_{1A}$}
				\bitbox{8}{$0.2*C_{1B}$}
				\bitbox{8}{$0.2*C_{1G}$}
				\bitbox{8}{$0.2*C_{1R}$}
				\bitbox{8}{$0.2*C_{1A}$}
				\bitbox{8}{$0.2*D_{1B}$}
				\bitbox{8}{$0.2*D_{1G}$}
				\bitbox{8}{$0.2*D_{1R}$}
				\bitbox{8}{$0.2*D_{1A}$}
			\end{bytefield}\\\vspace{1em}
		\end{center}
Luego cargamos en XMM11 los píxeles de la imagen fuente a partir del píxel$_{i-1,j-2}$ en XMM11, y a partir del píxel$_{i-1,j+2}$ en XMM10.
	    \begin{center}
			\textbf{XMM11}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitboxes{32}{{$p_{i-1,j-2}$}{$A_2$}{$B_2$}{$C_2$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
	    \begin{center}
			\textbf{XMM10}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitboxes{32}{{$D_2$}{$p_{i-1,j+3}$}{$p_{i-1,j+4}$}{$p_{i-1,j+5}$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
Los píxeles que necesito son $A_2$, $B_2$, $C_2$ y $D_2$. Entonces, utilizando la instrucción PSHUFB, junto los píxeles que necesito en XMM11.
	    \begin{center}
			\textbf{XMM11}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitboxes{32}{{$A_2$}{$B_2$}{$C_2$}{$D_2$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
A partir de acá se procesan los datos de la misma forma que los de la fila anterior. Sólo que el resultado obtenido en XMM11 se suma a los valores guardados en XMM0.
	    \begin{center}
			\textbf{XMM0 (bytes)}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=6em,endianness=little]{128}
				\bitheader{127,112,96,80,64,48,32,16,0}\\ 
				\bitbox{8}{$0.2*A_{1B} + 0.2*A_{2B}$}
				\bitbox{8}{$0.2*A_{1G} + 0.2*A_{2G}$}
				\bitbox{8}{$0.2*A_{1R} + 0.2*A_{2R}$}
				\bitbox{8}{$0.2*A_{1A} + 0.2*A_{2A}$}
				\bitbox{8}{$0.2*B_{1B} + 0.2*B_{2B}$}
				\bitbox{8}{$0.2*B_{1G} + 0.2*B_{2G}$}
				\bitbox{8}{$0.2*B_{1R} + 0.2*B_{2R}$}
				\bitbox{8}{$0.2*B_{1A} + 0.2*A_{2A}$}
				\bitbox{8}{$0.2*C_{1B} + 0.2*C_{2B}$}
				\bitbox{8}{$0.2*C_{1G} + 0.2*C_{2G}$}
				\bitbox{8}{$0.2*C_{1R} + 0.2*C_{2R}$}
				\bitbox{8}{$0.2*C_{1A} + 0.2*C_{2A}$}
				\bitbox{8}{$0.2*D_{1B} + 0.2*D_{2B}$}
				\bitbox{8}{$0.2*D_{1G} + 0.2*D_{2G}$}
				\bitbox{8}{$0.2*D_{1R} + 0.2*D_{2R}$}
				\bitbox{8}{$0.2*D_{1A} + 0.2*D_{2A}$}
			\end{bytefield}\\\vspace{1em}
		\end{center}
Luego cargamos en XMM11 los píxeles de la imagen fuente a partir del píxel$_{i,j-2}$ en XMM11, y a partir del píxel$_{i,j+2}$ en XMM10.
De nuevo, utilizando la instrucción PSHUFB, junto los píxeles que necesito en XMM11.
	    \begin{center}
			\textbf{XMM11}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.37em,bitheight=3em,endianness=little]{128}
				\bitboxes{32}{{$A_3$}{$B_3$}{$C_3$}{$D_3$}}\\
			\end{bytefield}\\\vspace{1em}
			\end{center}
A partir de acá se procesan los datos de la misma forma que los de la fila anterior. El resultado obtenido en XMM11 se suma a los valores guardados en XMM0.
	    \begin{center}
			\textbf{XMM0 (bytes)}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.74em,bitheight=6em,endianness=little]{64}
				\bitheader{63,48,32,16,0}\\ 
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $0.2*A_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*A_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*A_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*A_{iA}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*B_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*B_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*B_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*B_{iA}$}
			\end{bytefield}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.74em,bitheight=6em,endianness=little]{64}
				\bitheader[lsb=64]{127,112,96,80,64}\\ 
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*C_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*C_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*C_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*C_{iA}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*D_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*D_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*D_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{3}$\\ $ 0.2*D_{iA}$}
			\end{bytefield}\\\vspace{1em}
		\end{center}
Con los datos de los píxeles de la fila $i+1$ y posteriormente de la fila $i+2$, pertenecientes a las columnas $j-2$ a $j+5$ se realiza lo mismo que con los píxeles de las filas $i-2$ a $i$.
Finalmente, el resultado obtenido en XMM0 es:
	    \begin{center}
			\textbf{XMM0 (bytes)}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.74em,bitheight=6em,endianness=little]{64}
				\bitheader{63,48,32,16,0}\\ 
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $0.2*A_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*A_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*A_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*A_{iA}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*B_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*B_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*B_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*B_{iA}$}
			\end{bytefield}\\\vspace{1em}
			\begin{bytefield}[bitwidth=0.74em,bitheight=6em,endianness=little]{64}
				\bitheader[lsb=64]{127,112,96,80,64}\\ 
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*C_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*C_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*C_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*C_{iA}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*D_{iB}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*D_{iG}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*D_{iR}$}
				\bitbox{8}{$\sum_{i=1}^{5}$\\ $ 0.2*D_{iA}$}
			\end{bytefield}\\\vspace{1em}
		\end{center}
Este resultado se guarda en la imagen destino y se continúa con una nueva iteración.
